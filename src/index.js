import Store from "./store";
import Actions from "./actions";
import { showCurrentPlayer, drawToBoard, showWinner } from "./renderer";
import { checkWinner } from "./services";

const BOARD_SELECTOR = "[data-board]";
const NEW_GAME_SELECTOR = "button[data-new-game]";

const $board = document.querySelector(BOARD_SELECTOR);
const $newGameBtn = document.querySelector(NEW_GAME_SELECTOR);

function onClickBoard(e) {
  const { cellId } = e.target.dataset;
  Actions.playerTurn(cellId, Store.getState().currentPlayer);
}

$board.addEventListener("click", onClickBoard);

$newGameBtn.addEventListener("click", () => {
  Actions.newGame();
  $board.addEventListener("click", onClickBoard);
});

showCurrentPlayer(Store.getState().currentPlayer);

Store.addListener(() => {
  const { currentPlayer, matrix } = Store.getState();
  showCurrentPlayer(currentPlayer);
  drawToBoard(matrix);

  const winner = checkWinner(matrix);

  if (winner) {
    showWinner(winner);
    $board.removeEventListener("click", onClickBoard);
  }
});
