const ActionTypes = {
  PLAYER_TURN: "PLAYER_TURN",
  NEW_GAME: "NEW_GAME",
};

export default ActionTypes;
