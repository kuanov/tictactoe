import { ReduceStore } from "flux/utils";
import Dispatcher from "./dispatcher";
import ActionTypes from "./actionTypes";
import { transformToMatrixIndex } from "./services";

class AppStore extends ReduceStore {
  constructor() {
    super(Dispatcher);
  }

  getInitialState() {
    return {
      currentPlayer: 1,
      matrix: [
        [null, null, null],
        [null, null, null],
        [null, null, null],
      ],
    };
  }

  reduce(state, action) {
    switch (action.type) {
      case ActionTypes.PLAYER_TURN: {
        if (!action.cellId) return state;

        const [col, row] = transformToMatrixIndex(action.cellId);
        if (state.matrix[col][row]) return state;

        const updatedMatrix = [...state.matrix];
        updatedMatrix[col][row] = action.playerId;
        const currentPlayer = state.currentPlayer === 1 ? 2 : 1;

        return { currentPlayer, matrix: updatedMatrix };
      }

      case ActionTypes.NEW_GAME:
        return this.getInitialState();

      default:
        return state;
    }
  }
}

export default new AppStore();
