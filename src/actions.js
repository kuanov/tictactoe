import ActionTypes from "./actionTypes";
import Dispatcher from "./dispatcher";

const Actions = {
  playerTurn(cellId, playerId) {
    Dispatcher.dispatch({
      type: ActionTypes.PLAYER_TURN,
      cellId,
      playerId,
    });
  },
  newGame() {
    Dispatcher.dispatch({
      type: ActionTypes.NEW_GAME,
    });
  },
};

export default Actions;
