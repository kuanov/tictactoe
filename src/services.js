function transformToMatrixIndex(cellId) {
  return [Number(cellId[0]) - 1, Number(cellId[1]) - 1];
}

function checkWinner(matrix) {
  const flatMatrix = matrix
    .reduce((acc, cur) => [...acc, ...cur])
    .filter((el) => el);
  const firstPlayer = flatMatrix.filter((el) => el === 1);
  const secondPlayer = flatMatrix.filter((el) => el === 2);

  if (firstPlayer.length < 3 && secondPlayer.length < 3) return;

  const rowEqualsCount = {};
  const colEqualsCount = {};
  const diagonalEqualsCount = {};
  const reverseDiagonalEqualsCount = {};

  for (let col = 0; col < matrix.length; col += 1) {
    for (let row = 0; row < matrix[col].length; row += 1) {
      if (!matrix[col][row]) continue;

      if (
        row < matrix.length - 1 &&
        matrix[col][row] === matrix[col][row + 1]
      ) {
        colEqualsCount[col] = colEqualsCount[col] ? colEqualsCount[col] + 1 : 1;

        for (const key of Object.keys(colEqualsCount)) {
          if (colEqualsCount[key] === 2) {
            return matrix[key][0];
          }
        }
      }

      if (
        col < matrix.length - 1 &&
        matrix[col][row] === matrix[col + 1][row]
      ) {
        rowEqualsCount[row] = rowEqualsCount[row] ? rowEqualsCount[row] + 1 : 1;

        for (const key of Object.keys(rowEqualsCount)) {
          if (rowEqualsCount[key] === 2) {
            return matrix[0][key];
          }
        }
      }

      // diagonals
      if (
        col < matrix.length - 1 &&
        col === row &&
        matrix[col][row] === matrix[col + 1][row + 1]
      ) {
        diagonalEqualsCount[matrix[col][row]] = diagonalEqualsCount[
          matrix[col][row]
        ]
          ? diagonalEqualsCount[matrix[col][row]] + 1
          : 1;

        for (const key of Object.keys(diagonalEqualsCount)) {
          if (diagonalEqualsCount[key] === 2) {
            return key;
          }
        }
      }

      if (
        col < matrix.length - 1 &&
        row > 0 &&
        matrix[col][row] === matrix[col + 1][row - 1]
      ) {
        reverseDiagonalEqualsCount[matrix[col][row]] =
          reverseDiagonalEqualsCount[matrix[col][row]]
            ? reverseDiagonalEqualsCount[matrix[col][row]] + 1
            : 1;

        for (const key of Object.keys(reverseDiagonalEqualsCount)) {
          if (reverseDiagonalEqualsCount[key] === 2) {
            return key;
          }
        }
      }
    }
  }

  if (flatMatrix.length === 9) return -1;
}

export { transformToMatrixIndex, checkWinner };
