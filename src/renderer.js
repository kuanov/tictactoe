const FIRST_PLAYER_MARK = "first-player";
const SECOND_PLAYER_MARK = "second-player";
const INFO_SECTION_SELECTOR = "[data-info]";

const $infoSection = document.querySelector(INFO_SECTION_SELECTOR);

export function showCurrentPlayer(player) {
  $infoSection.textContent = `Ходит игрок №${player}`;
}

export function showWinner(winner) {
  $infoSection.textContent =
    winner > 0 ? `Победил игрок №${winner}` : "Ничья :)";
}

export function drawToBoard(matrix) {
  for (let col = 0; col < matrix.length; col += 1) {
    for (let row = 0; row < matrix[col].length; row += 1) {
      const targetCell = document.querySelector(
        `[data-cell-id="${col + 1}${row + 1}"]`
      );

      if (!matrix[col][row]) {
        targetCell.classList.remove(FIRST_PLAYER_MARK, SECOND_PLAYER_MARK);
      } else {
        matrix[col][row] === 1
          ? targetCell.classList.add(FIRST_PLAYER_MARK)
          : targetCell.classList.add(SECOND_PLAYER_MARK);
      }
    }
  }
}
