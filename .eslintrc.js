module.exports = {
  parser: "babel-eslint",
  extends: ["airbnb-base", "prettier"],
  env: {
    browser: true,
  },
  rules: {
    "no-console": 0,
    "no-restricted-syntax": 0,
    "import/prefer-default-export": "off",
    "no-unused-expressions": "off",
    "babel/no-unused-expressions": 0,
    "class-methods-use-this": "off",
    "react/prop-types": "off",
  },
};
